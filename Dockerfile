FROM python:alpine

RUN apk add --no-cache alpine-sdk npm nodejs
RUN pip install --upgrade pip
RUN pip install pyyaml toml xmltodict jinja2 jinja2-cli[yaml] jinja2-cli[toml] jinja2-cli[xml]
RUN npm update
RUN npm install --global --verbose \
    @babel/cli \
    @babel/core \
    @babel/preset-env \
    clean-css \
    clean-css-cli \
    coffeescript \
    handlebars \
    html-minifier \
    jasmine \
    karma \
    prettier \
    terser\
    typescript
RUN npm install --global --unsafe-perm --verbose node-sass
